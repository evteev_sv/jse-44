package com.evteev.jse;

import com.evteev.jse.controller.IAccountController;
import com.evteev.jse.controller.ICustomerController;
import com.evteev.jse.controller.impl.AccountControllerImpl;
import com.evteev.jse.controller.impl.CustomerControllerImpl;
import com.evteev.jse.dto.mapper.AccountMapper;
import com.evteev.jse.dto.mapper.CustomerMapper;
import com.evteev.jse.repository.IAccountRepository;
import com.evteev.jse.repository.ICustomerRepository;
import com.evteev.jse.repository.impl.AccountRepositoryImpl;
import com.evteev.jse.repository.impl.CustomerRepositoryImpl;
import com.evteev.jse.service.AccountService;
import com.evteev.jse.service.CustomerService;
import jakarta.xml.ws.Endpoint;

public class Application {

    public static void main(String[] args) {
        CustomerMapper customerMapper = CustomerMapper.INSTANCE;
        AccountMapper accountMapper = AccountMapper.INSTANCE;
        ICustomerRepository customerRepository = new CustomerRepositoryImpl();
        IAccountRepository accountRepository = new AccountRepositoryImpl();
        CustomerService customerService = new CustomerService(customerRepository, customerMapper);
        AccountService accountService = new AccountService(accountRepository, customerRepository, accountMapper);
        ICustomerController customerController = new CustomerControllerImpl(customerService);
        IAccountController accountController = new AccountControllerImpl(accountService);
        Endpoint.publish("http://localhost:8089/ws/customer", customerController);
        Endpoint.publish("http://localhost:8089/ws/account", accountController);
    }

}
