package com.evteev.jse.controller.impl;

import com.evteev.jse.controller.IAccountController;
import com.evteev.jse.dto.AccountDTO;
import com.evteev.jse.dto.ResponseAccountDTO;
import com.evteev.jse.service.AccountService;
import jakarta.jws.WebService;

@WebService(serviceName = "AccountService",
        portName = "AccountServicePort",
        endpointInterface = "com.evteev.jse.controller.IAccountController")
public class AccountControllerImpl implements IAccountController {

    private final AccountService accountService;

    public AccountControllerImpl(AccountService accountService) {
        this.accountService = accountService;
    }

    @Override
    public ResponseAccountDTO createAccount(AccountDTO accountDTO) {
        return accountService.create(accountDTO);
    }

    @Override
    public ResponseAccountDTO deleteAccount(AccountDTO accountDTO) {
        return accountService.delete(accountDTO);
    }

    @Override
    public ResponseAccountDTO updateAccount(AccountDTO accountDTO) {
        return accountService.update(accountDTO);
    }

}
