package com.evteev.jse.controller.impl;

import com.evteev.jse.controller.ICustomerController;
import com.evteev.jse.dto.CreateCustomerDTO;
import com.evteev.jse.dto.CustomerDTO;
import com.evteev.jse.dto.ResponseCustomerDTO;
import com.evteev.jse.dto.UpdateCustomerDTO;
import com.evteev.jse.service.CustomerService;
import jakarta.jws.WebService;

@WebService(serviceName = "CustomerService",
        portName = "CustomerServicePort",
        endpointInterface = "com.evteev.jse.controller.ICustomerController")
public class CustomerControllerImpl implements ICustomerController {

    private final CustomerService customerService;

    public CustomerControllerImpl(CustomerService customerService) {
        this.customerService = customerService;
    }

    @Override
    public ResponseCustomerDTO createCustomer(CreateCustomerDTO customerDTO) {
        return customerService.create(customerDTO);
    }

    @Override
    public ResponseCustomerDTO deleteCustomer(Long id) {
        return customerService.delete(id);
    }

    @Override
    public ResponseCustomerDTO updateCustomer(UpdateCustomerDTO customerDTO) {
        return customerService.update(customerDTO);
    }

    @Override
    public ResponseCustomerDTO getCustomer(CustomerDTO customerDTO) {
        return customerService.findCustomer(customerDTO);
    }
}
