package com.evteev.jse.dto;

import com.evteev.jse.enumeration.Status;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OperationStatus implements Serializable {

    private static final long serialVersionUID = 1L;

    private Status status;
    private String message;

}
