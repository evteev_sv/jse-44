package com.evteev.jse.dto;

import jakarta.xml.bind.annotation.XmlAccessType;
import jakarta.xml.bind.annotation.XmlAccessorType;
import jakarta.xml.bind.annotation.XmlElement;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
public class ResponseAccountDTO implements Serializable {

    private static final long serialVersionUID = 1L;

    @XmlElement(name = "OperationStatus")
    private OperationStatus operationStatus;
    @XmlElement(name = "Account")
    private AccountDTO accountDTO;

}
