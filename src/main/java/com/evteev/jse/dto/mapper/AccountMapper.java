package com.evteev.jse.dto.mapper;

import com.evteev.jse.dto.AccountDTO;
import com.evteev.jse.model.Account;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface AccountMapper {

    AccountMapper INSTANCE = Mappers.getMapper(AccountMapper.class);

    Account accountDTOtoAccount(AccountDTO accountDTO);

    AccountDTO accountToAccountDTO(Account account);

}
