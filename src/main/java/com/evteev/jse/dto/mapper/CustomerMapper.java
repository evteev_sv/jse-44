package com.evteev.jse.dto.mapper;

import com.evteev.jse.dto.CreateCustomerDTO;
import com.evteev.jse.dto.CustomerDTO;
import com.evteev.jse.dto.UpdateCustomerDTO;
import com.evteev.jse.model.Customer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface CustomerMapper {

    CustomerMapper INSTANCE = Mappers.getMapper(CustomerMapper.class);

    @Mapping(source = "birthDate", target = "birthDate", dateFormat = "yyyy-MM-dd")
    Customer customerDTOtoCustomer(CustomerDTO customerDTO);

    @Mapping(source = "birthDate", target = "birthDate", dateFormat = "yyyy-MM-dd")
    Customer customerDTOtoCustomer(CreateCustomerDTO customerDTO);

    @Mapping(source = "birthDate", target = "birthDate", dateFormat = "yyyy-MM-dd")
    Customer customerDTOtoCustomer(UpdateCustomerDTO customerDTO);

    @Mapping(source = "birthDate", target = "birthDate", dateFormat = "yyyy-MM-dd")
    CustomerDTO customerToCustomerDTO(Customer customer);

    @Mapping(source = "birthDate", target = "birthDate", dateFormat = "yyyy-MM-dd")
    List<CustomerDTO> customerListToCustomerListDTO(List<Customer> customers);

}
