package com.evteev.jse.enumeration;

public enum Status {

    OK("Ok"),
    ERROR("Error");

    private String message;

    Status(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

}
