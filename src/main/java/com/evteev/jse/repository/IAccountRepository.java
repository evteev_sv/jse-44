package com.evteev.jse.repository;

import com.evteev.jse.model.Account;

import java.util.Optional;

public interface IAccountRepository {

    Optional<Account> create(Account account);

    Optional<Account> getByIdAndAccountNumber(Account account);

    void update(Account account);

    void delete(Account account);

}
