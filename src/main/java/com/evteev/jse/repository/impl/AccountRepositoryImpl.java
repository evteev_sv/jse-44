package com.evteev.jse.repository.impl;

import com.evteev.jse.model.Account;
import com.evteev.jse.repository.IAccountRepository;
import com.evteev.jse.repository.datasource.MyBatisUtil;
import com.evteev.jse.repository.impl.mapper.IAccountMapper;
import org.apache.ibatis.session.SqlSession;

import java.util.Optional;

public class AccountRepositoryImpl implements IAccountRepository {
    @Override
    public Optional<Account> create(Account account) {
        try (SqlSession session = MyBatisUtil.getSqlSession()) {
            IAccountMapper accountMapper = session.getMapper(IAccountMapper.class);
            accountMapper.create(account);
            session.commit();
            return Optional.of(account);
        }
    }

    @Override
    public Optional<Account> getByIdAndAccountNumber(Account account) {
        try (SqlSession session = MyBatisUtil.getSqlSession()) {
            IAccountMapper accountMapper = session.getMapper(IAccountMapper.class);
            if (accountMapper.getByIdAndAccountNumber(account) == null) {
                return Optional.empty();
            }
            return Optional.of(accountMapper.getByIdAndAccountNumber(account));
        }
    }

    @Override
    public void update(Account account) {
        try (SqlSession session = MyBatisUtil.getSqlSession()) {
            IAccountMapper accountMapper = session.getMapper(IAccountMapper.class);
            accountMapper.update(account);
            session.commit();
        }
    }

    @Override
    public void delete(Account account) {
        try (SqlSession session = MyBatisUtil.getSqlSession()) {
            IAccountMapper accountMapper = session.getMapper(IAccountMapper.class);
            accountMapper.delete(account);
            session.commit();
        }
    }

}
