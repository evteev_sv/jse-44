package com.evteev.jse.repository.impl;

import com.evteev.jse.model.Customer;
import com.evteev.jse.repository.ICustomerRepository;
import com.evteev.jse.repository.datasource.MyBatisUtil;
import com.evteev.jse.repository.impl.mapper.ICustomerMapper;
import lombok.extern.log4j.Log4j2;
import org.apache.ibatis.session.SqlSession;

import java.util.List;
import java.util.Optional;

@Log4j2
public class CustomerRepositoryImpl implements ICustomerRepository {

    @Override
    public Optional<Customer> create(Customer customer) {
        try (SqlSession session = MyBatisUtil.getSqlSession()) {
            ICustomerMapper customerMapper = session.getMapper(ICustomerMapper.class);
            customerMapper.create(customer);
            session.commit();
            return Optional.of(customer);
        }
    }

    @Override
    public Optional<Customer> getById(Long id) {
        try (SqlSession session = MyBatisUtil.getSqlSession()) {
            ICustomerMapper customerMapper = session.getMapper(ICustomerMapper.class);
            if (customerMapper.getById(id) == null) {
                return Optional.empty();
            }
            return Optional.of(customerMapper.getById(id));
        }
    }

    @Override
    public List<Customer> getCustomer(Customer customer) {
        try (SqlSession session = MyBatisUtil.getSqlSession()) {
            ICustomerMapper customerMapper = session.getMapper(ICustomerMapper.class);
            return customerMapper.getCustomer(customer);
        }
    }

    @Override
    public void update(Customer customer) {
        try (SqlSession session = MyBatisUtil.getSqlSession()) {
            ICustomerMapper customerMapper = session.getMapper(ICustomerMapper.class);
            customerMapper.update(customer);
            session.commit();
        }
    }

    @Override
    public void delete(Long id) {
        try (SqlSession session = MyBatisUtil.getSqlSession()) {
            ICustomerMapper customerMapper = session.getMapper(ICustomerMapper.class);
            customerMapper.delete(id);
            session.commit();
        }
    }

}
