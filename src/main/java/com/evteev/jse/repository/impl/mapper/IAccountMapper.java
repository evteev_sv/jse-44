package com.evteev.jse.repository.impl.mapper;

import com.evteev.jse.model.Account;
import org.apache.ibatis.annotations.*;

public interface IAccountMapper {

    @Insert("insert into account(account_number,ballance,customer_id) values(#{accountNumber},#{ballance},#{customerId})")
    @Options(keyProperty = "customer_id")
    Integer create(Account account);

    @Update("update account set ballance=#{ballance} where customer_id = #{customerId} and account_number = #{accountNumber}")
    void update(Account account);

    @Delete("delete from account where customer_id=#{customerId} and account_number = #{accountNumber}")
    void delete(Account account);

    @Select("select * from account where customer_id=#{customerId} and account_number = #{accountNumber}")
    @Results({
            @Result(property = "accountNumber", column = "account_number"),
            @Result(property = "customerId", column = "customer_id"),
            @Result(property = "ballance", column = "ballance")
    })
    Account getByIdAndAccountNumber(Account account);


}
