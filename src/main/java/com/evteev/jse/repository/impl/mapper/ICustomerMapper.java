package com.evteev.jse.repository.impl.mapper;

import com.evteev.jse.model.Customer;
import com.evteev.jse.repository.impl.mapper.provider.CustomerProvider;
import org.apache.ibatis.annotations.*;

import java.util.List;

public interface ICustomerMapper {

    @Insert("insert into customer(birth_date,first_name,last_name) values(to_date(cast(#{birthDate} as text),'YYYY-MM-DD'),#{firstName},#{lastName})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    Integer create(Customer customer);

    @Update("update customer set birth_date = to_date(cast(#{birthDate} as text),'YYYY-MM-DD'),first_name=#{firstName},last_name=#{lastName} where id = #{id}")
    void update(Customer customer);

    @Delete("delete from customer where id=#{id}")
    void delete(Long id);

    @Select("select * from customer where id=#{id}")
    @Results({
            @Result(property = "birthDate", column = "birth_date"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
    })
    Customer getById(Long id);

    @SelectProvider(type = CustomerProvider.class, method = "getCustomer")
    @Results({
            @Result(property = "birthDate", column = "birth_date"),
            @Result(property = "firstName", column = "first_name"),
            @Result(property = "lastName", column = "last_name"),
            @Result(property = "account.accountNumber", column = "account_number"),
            @Result(property = "account.ballance", column = "ballance")
    })
    List<Customer> getCustomer(Customer customer);

}
