package com.evteev.jse.repository.impl.mapper.provider;

import com.evteev.jse.model.Customer;
import org.apache.ibatis.jdbc.SQL;

public class CustomerProvider {

    public String getCustomer(Customer customer) {
        return new SQL() {{
            SELECT(" customer.id, customer.first_name, customer.last_name, customer.birth_date, account.account_number, account.ballance");
            FROM("customer");
            LEFT_OUTER_JOIN("account on customer.id=account.customer_id");
            boolean hasCondition = false;
            if (customer.getFirstName() != null) {
                WHERE("first_name='" + customer.getFirstName() + "'");
                hasCondition = true;
            }
            if (customer.getLastName() != null) {
                if (hasCondition) {
                    AND();
                }
                WHERE("last_name='" + customer.getLastName() + "'");
                hasCondition = true;
            }
            if (customer.getBirthDate() != null) {
                if (hasCondition) {
                    AND();
                }
                WHERE("birth_date=to_date(cast('" + customer.getBirthDate() + "' as text),'YYYY-MM-DD')");
            }
            if (customer.getAccount() != null) {
                if (customer.getAccount().getAccountNumber() != null) {
                    if (hasCondition) {
                        AND();
                    }
                    WHERE("account_number='" + customer.getAccount().getAccountNumber() + "'");
                    hasCondition = true;
                }
                if (customer.getAccount().getBalance() != null) {
                    if (hasCondition) {
                        AND();
                    }
                    WHERE("ballance='" + customer.getAccount().getBalance() + "'");
                }
            }

        }}.toString();
    }

}
