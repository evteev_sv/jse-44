package com.evteev.jse.service;

import com.evteev.jse.dto.AccountDTO;
import com.evteev.jse.dto.OperationStatus;
import com.evteev.jse.dto.ResponseAccountDTO;
import com.evteev.jse.dto.mapper.AccountMapper;
import com.evteev.jse.enumeration.Status;
import com.evteev.jse.model.Account;
import com.evteev.jse.model.Customer;
import com.evteev.jse.repository.IAccountRepository;
import com.evteev.jse.repository.ICustomerRepository;

import java.util.Optional;

public class AccountService {

    private static final String EMPTY_ID = "Empty id";
    private static final String EMPTY_ACCOUNT_NUMBER = "Empty account number";
    private static final String EMPTY_BALANCE = "Empty balance";
    private static final String NOT_FOUND = "Account not found";
    private final IAccountRepository accountRepository;
    private final ICustomerRepository customerRepository;
    private final AccountMapper mapper;

    public AccountService(IAccountRepository accountRepository, ICustomerRepository customerRepository, AccountMapper mapper) {
        this.accountRepository = accountRepository;
        this.customerRepository = customerRepository;
        this.mapper = mapper;
    }

    public ResponseAccountDTO create(AccountDTO accountDTO) {
        if (accountDTO.getCustomerId() == null) {
            return errorBuilder(EMPTY_ID);
        }
        if (accountDTO.getAccountNumber() == null || accountDTO.getAccountNumber().isEmpty()) {
            return errorBuilder(EMPTY_ACCOUNT_NUMBER);
        }
        if (accountDTO.getBalance() == null) {
            return errorBuilder(EMPTY_BALANCE);
        }
        Optional<Customer> customerOptional = customerRepository.getById(accountDTO.getCustomerId());
        if (customerOptional.isEmpty()) {
            return errorBuilder("Customer does not exist with id " + accountDTO.getCustomerId());
        }
        Optional<Account> duplicate = accountRepository.getByIdAndAccountNumber(mapper.accountDTOtoAccount(accountDTO));
        if (duplicate.isPresent()) {
            return errorBuilder("The account number already exists with this customer");
        }
        Optional<Account> accountOptional = accountRepository.create(mapper.accountDTOtoAccount(accountDTO));
        if (accountOptional.isPresent()) {
            return responseBuilder(accountOptional.get());
        }
        return errorBuilder("Creation customer failed");
    }

    public ResponseAccountDTO update(AccountDTO accountDTO) {
        if (accountDTO.getCustomerId() == null) {
            return errorBuilder(EMPTY_ID);
        }
        if (accountDTO.getAccountNumber() == null || accountDTO.getAccountNumber().isEmpty()) {
            return errorBuilder(EMPTY_ACCOUNT_NUMBER);
        }
        if (accountDTO.getBalance() == null) {
            return errorBuilder(EMPTY_BALANCE);
        }
        Optional<Account> accountOptional = accountRepository.getByIdAndAccountNumber(mapper.accountDTOtoAccount(accountDTO));
        if (accountOptional.isEmpty()) {
            return errorBuilder(NOT_FOUND);
        }
        accountRepository.update(mapper.accountDTOtoAccount(accountDTO));
        return responseBuilder();
    }

    public ResponseAccountDTO delete(AccountDTO accountDTO) {
        if (accountDTO.getCustomerId() == null) {
            return errorBuilder(EMPTY_ID);
        }
        if (accountDTO.getAccountNumber() == null || accountDTO.getAccountNumber().isEmpty()) {
            return errorBuilder(EMPTY_ACCOUNT_NUMBER);
        }
        Optional<Account> accountOptional = accountRepository.getByIdAndAccountNumber(mapper.accountDTOtoAccount(accountDTO));
        if (accountOptional.isEmpty()) {
            return errorBuilder(NOT_FOUND);
        }
        accountRepository.delete(mapper.accountDTOtoAccount(accountDTO));
        return responseBuilder();
    }

    private ResponseAccountDTO errorBuilder(String message) {
        return ResponseAccountDTO.builder()
                .operationStatus(OperationStatus.builder().status(Status.ERROR).message(message).build())
                .build();
    }

    private ResponseAccountDTO responseBuilder() {
        return ResponseAccountDTO.builder()
                .operationStatus(OperationStatus.builder().status(Status.OK).build())
                .build();
    }

    private ResponseAccountDTO responseBuilder(Account account) {
        return ResponseAccountDTO.builder()
                .accountDTO(mapper.accountToAccountDTO(account))
                .build();
    }

}
