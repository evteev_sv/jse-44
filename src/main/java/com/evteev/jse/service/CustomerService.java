package com.evteev.jse.service;

import com.evteev.jse.dto.*;
import com.evteev.jse.dto.mapper.CustomerMapper;
import com.evteev.jse.enumeration.Status;
import com.evteev.jse.model.Customer;
import com.evteev.jse.repository.ICustomerRepository;

import java.util.List;
import java.util.Optional;

public class CustomerService {

    private static final String EMPTY_ID = "Empty id";
    private static final String NOT_FOUND = "Customer not found";
    private final ICustomerRepository customerRepository;
    private final CustomerMapper mapper;

    public CustomerService(ICustomerRepository customerRepository, CustomerMapper mapper) {
        this.customerRepository = customerRepository;
        this.mapper = mapper;
    }

    public ResponseCustomerDTO create(CreateCustomerDTO customerDTO) {
        Optional<Customer> customerOptional = customerRepository.create(mapper.customerDTOtoCustomer(customerDTO));
        if (customerOptional.isPresent()) {
            return responseBuilder(customerOptional.get());
        }
        return errorBuilder("Creation customer failed");
    }

    public ResponseCustomerDTO update(UpdateCustomerDTO customerDTO) {
        if (customerDTO.getId() == null || customerDTO.getBirthDate().isEmpty()) {
            return errorBuilder(EMPTY_ID);
        }
        Optional<Customer> customerOptional = customerRepository.getById(customerDTO.getId());
        if (customerOptional.isEmpty()) {
            return errorBuilder(NOT_FOUND);
        }
        customerRepository.update(mapper.customerDTOtoCustomer(customerDTO));
        return responseBuilder();
    }

    public ResponseCustomerDTO delete(Long id) {
        if (id == null) {
            return errorBuilder(EMPTY_ID);
        }
        Optional<Customer> customerOptional = customerRepository.getById(id);
        if (customerOptional.isEmpty()) {
            return errorBuilder(NOT_FOUND);
        }
        customerRepository.delete(id);
        return responseBuilder();
    }

    public ResponseCustomerDTO findCustomer(CustomerDTO customerDTO) {
        List<Customer> customerList = customerRepository.getCustomer(mapper.customerDTOtoCustomer(customerDTO));
        return responseBuilder(customerList);
    }

    private ResponseCustomerDTO errorBuilder(String message) {
        return ResponseCustomerDTO.builder()
                .operationStatus(OperationStatus.builder().status(Status.ERROR).message(message).build())
                .build();
    }

    private ResponseCustomerDTO responseBuilder() {
        return ResponseCustomerDTO.builder()
                .operationStatus(OperationStatus.builder().status(Status.OK).build())
                .build();
    }

    private ResponseCustomerDTO responseBuilder(Customer customer) {
        return ResponseCustomerDTO.builder()
                .customerDTO(new CustomerDTO[]{mapper.customerToCustomerDTO(customer)})
                .build();
    }

    private ResponseCustomerDTO responseBuilder(List<Customer> projectList) {
        return ResponseCustomerDTO.builder()
                .customerDTO(mapper.customerListToCustomerListDTO(projectList).toArray(new CustomerDTO[projectList.size()]))
                .build();
    }

}
